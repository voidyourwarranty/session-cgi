/* -*- mode:c++ -*-
// ================================================================================
// GitLab:    http://gitlab.com/voidyourwarranty/session-cgi
// File:      protocol_session_cgi.c
// Date:      2018-10-10
// Author:    voidyourwarranty
// License:   Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
// Purpose:   Plugin for the LibWebsockets web server that implements a session CGI
// ================================================================================
*/

#if !defined (LWS_PLUGIN_STATIC)
#define LWS_DLL
#define LWS_INTERNAL
#include <libwebsockets.h>
#endif

#define FDSTDIN  0          /* stdin of the CGI session handler processes */
#define FDSTDOUT 1          /* ... stdout */
#define FDSTDERR 2          /* ... stderr */
#define READ  0             /* for each pipe, 0 is read */
#define WRITE 1             /* ... and 1 is write */

#define SIZE_BUFFER   65536 /* size of internal message buffer in bytes */
#define MAX_HDR_TOKEN   100 /* the maximum numeric value of lws_token_indexes or more */

#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

/*
// Fork off a sub-process that serves as a CGI session handler, that receives all web socket messages from a given
// client into its standard input and whose standard output is sent back to the web socket client.
//
// The executable and its command line arguments are given in <argv> which is a NULL terminated array of strings. The
// standard input, output and error of the CGI session handler are redirected to pipes. The file descriptors in order to
// access these pipes are are returned to <in> (writeable), <out> (readable) and <err> (readable).
//
// The function returns -1 in case of an error and otherwise the process id of the CGI session handler.
//
// The present plugin consecutively numbers its sessions. The session number whose handler is to be forked off, is
// passed as <session>.
*/

int fork_cgi ( int session, int *in, int *out, int *err, const char **argv ) {
  
  int pid;         /* ID of the child process after forking */
  int pipes[3][2]; /* 3 pipes with 2 directions */
  
  if (pipe (pipes[FDSTDIN]) < 0) {
    lwsl_err ("session-cgi: unable to create stdin pipe of session %d.\n",session);
    return (-1);
  }

  if (pipe (pipes[FDSTDOUT]) < 0) {
    close (pipes[FDSTDIN][READ]);
    close (pipes[FDSTDIN][WRITE]);
    lwsl_err ("session-cgi: unable to create stdout pipe of session %d.\n",session);
    return (-1);
  }

  if (pipe (pipes[FDSTDERR]) < 0) {
    close (pipes[FDSTDIN][READ]);
    close (pipes[FDSTDIN][WRITE]);
    close (pipes[FDSTDOUT][READ]);
    close (pipes[FDSTDOUT][WRITE]);
    lwsl_err ("session-cgi: unable to create stderr pipe of session %d.\n",session);
    return (-1);
  }
  
  pid = fork ();
  if (pid > 0) { /* parent */

    close (pipes[FDSTDIN][READ]);
    close (pipes[FDSTDOUT][WRITE]);
    close (pipes[FDSTDERR][WRITE]);
    
    *in  = pipes[FDSTDIN][WRITE];
    *out = pipes[FDSTDOUT][READ];
    *err = pipes[FDSTDERR][READ];
    
    return (pid);
    
  } else if (pid == 0) { /* child */

    close (pipes[FDSTDIN][WRITE]);
    close (pipes[FDSTDOUT][READ]);
    close (pipes[FDSTDERR][READ]);

    dup2 (pipes[FDSTDIN][READ],STDIN_FILENO);
    dup2 (pipes[FDSTDOUT][WRITE],STDOUT_FILENO);
    dup2 (pipes[FDSTDERR][WRITE],STDERR_FILENO);
    
    execv(argv[0],(char * const *)argv);
    {
      /*
      // This point is reached only if <execv> failed. Since the parent can write to the log file at any time, we don't
      // use the log directly (this wouldn't be safe because of concurrent write access to a shared resource), but
      // rather emit the error message to standard error where the parent process will find it.
      */

      char *err = (char *)malloc (strlen (argv[0])+200);
      int   n;
      sprintf (err,"unable to execute '%s' as the handler for session %d.\n",argv[0],session);
      n = write (STDERR_FILENO,err,strlen (err));
      free (err);
    }
    _exit (1);
    
  } else { /* error on fork */
    
    close (pipes[FDSTDIN][READ]);
    close (pipes[FDSTDIN][WRITE]);
    close (pipes[FDSTDOUT][READ]);
    close (pipes[FDSTDOUT][WRITE]);
    close (pipes[FDSTDERR][READ]);
    close (pipes[FDSTDERR][WRITE]);
    lwsl_err ("session-cgi: unable to fork the handler for session %d.\n",session);
    return (-1);
  }
}

/*
// Close the three file descriptors <in>, <out> and <err> that belong to a sub process <pid> and send it a SIGTERM
// signal.
*/

void kill_cgi ( int pid, int in, int out, int err ) {

  close (in);
  close (out);
  close (err);

  kill (pid,SIGTERM);
}

/*
// A linked list of the data that are specific to the individual sessions (i.e. web socket connection to individual
// clients).
*/

struct per_session_data__session_cgi {
  struct per_session_data__session_cgi *pss_list; /* linked list */
  struct lws                           *wsi;      /* LibWebsockets identification of the session */

  int                                   number;   /* number of the session, internal to this protocol */
  int                                   pid;      /* process ID of the CGI session handler process */
  int                                   stdin;    /* file descriptor to reach its standard input (writeable) */
  int                                   stdout;   /* ... standard output (readable) */
  int                                   stderr;   /* ... standard error (readable) */
  char                                 *buffer;   /* message buffer */
};

/*
// The structure for each vhost of the LibWebsockets Web Server that is used with the present protocol.
*/

struct per_vhost_data__session_cgi {
  struct lws_context                   *context;  /* LibWebsockets context */
  struct lws_vhost                     *vhost;    /* LibWebsockets vhost */     
  const struct lws_protocols           *protocol; /* LibWebsockets protocol */
  struct per_session_data__session_cgi *pss_list; /* linked list of open sessions */

  int                                   counter;  /* number of the next session to be opened */
  int                                   total;    /* number of currently active sessions */
  const char                           *handler;  /* the full path to the executable that serves as the CGI session handler */
  const char                           *config;   /* the per-vhost configuration for the CGI session handler */
};

/*
// The main call back function of LibWebsockets.
*/

static int callback_session_cgi ( struct lws                *wsi,
				  enum lws_callback_reasons  reason,
				  void                      *user,
				  void                      *in,
				  size_t                     len    ) {
  
  const struct lws_protocol_vhost_options    *pvo = (const struct lws_protocol_vhost_options *)in; /* per protocol options */
  struct       per_session_data__session_cgi *pss = (struct per_session_data__session_cgi *)user;  /* per session data */
  struct       per_vhost_data__session_cgi   *vhd = (struct per_vhost_data__session_cgi *)         /* per vhost data */
    lws_protocol_vh_priv_get (lws_get_vhost (wsi),lws_get_protocol (wsi));
  int                                         m,n;

  switch (reason) {
    
  case LWS_CALLBACK_PROTOCOL_INIT: /* initialize the protocol for a vhost */

    /* allocate and initialize the per-vhost per-protocol data */
    vhd = lws_protocol_vh_priv_zalloc (lws_get_vhost (wsi),lws_get_protocol (wsi),sizeof (struct per_vhost_data__session_cgi));
    vhd->context  = lws_get_context(wsi);
    vhd->protocol = lws_get_protocol(wsi);
    vhd->vhost    = lws_get_vhost(wsi);
    vhd->counter  = 0;
    vhd->total    = 0;

    /* parse the per-protocol options of the vhost configuration */
    vhd->handler = NULL;
    vhd->config  = "{}";
    while (pvo) {
      if (!strcmp (pvo->name,"application"))
	vhd->handler = pvo->value;
      else if (!strcmp (pvo->name,"config"))
	vhd->config = pvo->value;
      else if (strcmp (pvo->name,"status"))
	lwsl_notice ("session-cgi: ignoring per vhost parameter '%s'.\n",pvo->name);
      
      pvo = pvo->next;
    }
    if (!vhd->handler) {
      lwsl_err ("session-cgi: missing per vhost parameter 'application'.\n");
      return (-1);
    }
    break;
    
  case LWS_CALLBACK_ESTABLISHED: /* new session opened */

    /* add to the list of open sessions and initialize the per-session data */
    lws_ll_fwd_insert (pss,pss_list,vhd->pss_list);
    pss->wsi    = wsi;
    pss->number = vhd->counter++;
    pss->pid    = -1;
    pss->stdin  = -1;
    pss->stdout = -1;
    pss->stderr = -1;
    pss->buffer = (char *)malloc (SIZE_BUFFER);
    
    lws_get_peer_simple (wsi,pss->buffer,SIZE_BUFFER);
    lwsl_notice ("session-cgi: new session %d; now %d sessions active; vhost %s; port %d; IP %s.\n",
		 pss->number,++vhd->total,lws_get_vhost_name (lws_get_vhost (wsi)),
		 lws_get_vhost_port (lws_get_vhost (wsi)),pss->buffer);

    { 
      /* fork off the CGI session handler process */
      const char *argv[] = { vhd->handler, 0 };
      pss->pid = fork_cgi (pss->number,&pss->stdin,&pss->stdout,&pss->stderr,argv);
      if (pss->pid < 0) {
	return (-1); /* close this session */
      } else {
	lwsl_notice ("session-cgi: forked CGI session %d handler '%s' with PID %d.\n",pss->number,argv[0],pss->pid);

	/* we regularly read from the CGI session handler's standard output and error, and we need them non-blocking */
	if (fcntl (pss->stdout,F_SETFL,O_NONBLOCK) < 0) {
	  lwsl_err ("session-cgi: unable to make stdout of session %d non-blocking.\n",pss->number);
	  return (-1); /* close this session */
	}
	
	if (fcntl (pss->stderr,F_SETFL,O_NONBLOCK) < 0) {
	  lwsl_err ("session-cgi: unable to make stderr of session %d non-blocking.\n",pss->number);
	  return (-1); /* close this session */
	}

	/*
	// Send to the CGI session handler three JSON objects: the session data (type: "session"), the HTTP headers
	// (type: "headers"), and the user configuration (type: "config").
	*/
	
	{
	  char json[SIZE_BUFFER];
	  int  i,h,d;

	  if (2*strlen (lws_get_vhost_name (lws_get_vhost (wsi))) + 2*strlen (pss->buffer) + 80 > SIZE_BUFFER) {
	    lwsl_err ("session-cgi: insufficient buffer for 'session' JSON object.\n");
	  } else {
	    sprintf (json,"{ \"type\": \"session\", \"session\": %d, \"pid\": %d, \"port\": %d, \"vhost\": \"",
		     pss->number,pss->pid,lws_get_vhost_port (lws_get_vhost (wsi)));
	    i = strlen (json);
	    lws_json_purify ((char *)json + i,lws_get_vhost_name (lws_get_vhost (wsi)),SIZE_BUFFER-1-i);
	    i = strlen (json);
	    strncpy ((char *)json + i,"\", \"peer\": \"",SIZE_BUFFER-1-i);
	    i = strlen (json);
	    lws_json_purify ((char *)json + i,pss->buffer,SIZE_BUFFER-1-i);
	    i = strlen (json);
	    strncpy ((char *)json + i,"\" }\n",SIZE_BUFFER-1-i);

	    if (write (pss->stdin,json,strlen (json)) < strlen (json)) 
	      lwsl_err ("session-cgi: write error to CGI session %d handler's <stdin>.\n",pss->number);
	  }

	  if (strlen (vhd->config) + 20 > SIZE_BUFFER) {
	    lwsl_err ("session-cgi: insufficient buffer for 'config' JSON object.\n");
	  } else {
	    strcpy (json,"{ \"type\": \"config\", \"config\": ");
	    i = strlen (json);
	    strncpy ((char *)json + i,vhd->config,SIZE_BUFFER-1-i);
	    i = strlen (json);
	    strncpy ((char *)json + i," }\n",SIZE_BUFFER-1-i);

	    if (write (pss->stdin,json,strlen (json)) < strlen (json)) 
	      lwsl_err ("session-cgi: write error to CGI session %d handler's <stdin>.\n",pss->number);
	  }
	  
	  strcpy (json,"{ \"type\": \"headers\", \"headers\": { ");
	  i = strlen (json);
	  d = 0;
	  h = 0;
	  do{
	    if (lws_hdr_copy (wsi,pss->buffer,SIZE_BUFFER-1,h) > 0) {

	      if (2*strlen (lws_token_to_string (h)) + 2*strlen (pss->buffer) + 20 > SIZE_BUFFER) {
		lwsl_err ("session-cgi: insufficient buffer for HTTP header '%s' of size %ld.\n",
			  lws_token_to_string (h),strlen (pss->buffer));
	      } else {

		if (2*strlen (lws_token_to_string (h)) + 2*strlen (pss->buffer) + 20 + i > SIZE_BUFFER) {
		  if (write (pss->stdin,json,strlen (json)) < strlen (json)) {
		    lwsl_err ("session-cgi: write error to CGI session %d handler's <stdin>.\n",pss->number);
		  }
		  i = 0;
		}
		
		if (d) {
		  strncpy ((char *)json + i,", ",SIZE_BUFFER-1-i);
		  i = strlen (json);
		}

		strncpy ((char *)json + i,"\"",SIZE_BUFFER-1-i);
		i = strlen (json);
		lws_json_purify ((char *)json + i,lws_token_to_string (h),SIZE_BUFFER-1-i);
		i = strlen (json);
		strncpy ((char *)json + i,"\": \"",SIZE_BUFFER-1-i);
		i = strlen (json);
		lws_json_purify ((char *)json + i,pss->buffer,SIZE_BUFFER-1-i);
		i = strlen (json);
		strncpy ((char *)json + i,"\"",SIZE_BUFFER-1-i);
		i = strlen (json);
		d = 1;
	      }
	    }
	  }while (h++ <= MAX_HDR_TOKEN);

	  strncpy ((char *)json + i," } }\n",SIZE_BUFFER-1-i);
	  if (write (pss->stdin,json,strlen (json)) < strlen (json)) {
	    lwsl_err ("session-cgi: write error to CGI session %d handler's <stdin>.\n",pss->number);
	  }
	}
      }
    }
    break;
    
  case LWS_CALLBACK_CLOSED: /* session closed */

    kill_cgi (pss->pid,pss->stdin,pss->stdout,pss->stderr);

    free (pss->buffer);
    pss->buffer = NULL;

    m = pss->number;
    
    lws_ll_fwd_remove (struct per_session_data__session_cgi,pss_list,pss,vhd->pss_list); /* remove from the list of open sessions */

    /* double-check the number of open sessions */
    n = 0;
    pss = vhd->pss_list;
    while (pss) {
      pss = pss->pss_list;
      n++;
    }
    vhd->total = n;

    lwsl_notice ("session-cgi: session %d closed; %d sessions still active.\n",m,n);
    break;

  case LWS_CALLBACK_SERVER_WRITEABLE: /* a client is ready to receive, please send */

    /*
    // First, we deal with standard error output of the CGI session handler. This output is merely logged, and so it
    // does not affect the single call to <lws_write ()> that we are allowed in this call-back.
    */

    if (pss->stderr > 0) {
      n = read (pss->stderr,pss->buffer,SIZE_BUFFER-1);
      while (n > 0) { 
	pss->buffer[n] = 0;
	lwsl_err ("session-cgi: CGI session %d handler: %s",pss->number,pss->buffer);
	
	n = read (pss->stderr,pss->buffer,SIZE_BUFFER-1);
      }

      if (n == 0) {
	lwsl_notice ("session-cgi: CGI session %d handler closed <stderr>.\n",pss->number);
	return (-1); /* close this session */
      } else if (n < 0) {
	if (errno != EAGAIN) {
	  lwsl_err ("session-cgi: read error from CGI session %d handler's <stderr>.\n",pss->number);
	  return (-1); /* close this session */
	}
      }
    }
    
    /*
    // Then we send whatever we receive from the CGI session handler's standard output.
    */

    if (pss->stdout > 0) {
      n = read (pss->stdout,pss->buffer+LWS_PRE,SIZE_BUFFER-LWS_PRE);
      if (n < 0) {
	if (errno != EAGAIN) {
	  lwsl_err ("session-cgi: read error from CGI session %d handler's <stdout>.\n",pss->number);
	  return (-1); /* close this session */
	}
      } else if (n == 0) {
	lwsl_notice ("session-cgi: CGI session %d handler closed <stdout>.\n",pss->number);
	return (-1); /* close this session */
      } else {
	m = lws_write (wsi,pss->buffer+LWS_PRE,n,LWS_WRITE_TEXT);
	if (m < n) {
	  lwsl_err ("session-cgi: write error to session %d Web Socket.\n",pss->number);
	  return (-1); /* close this session */
	}
      }
    }
    
    lws_callback_on_writable (wsi); /* notify that we are ready for further data */
    break;
    
  case LWS_CALLBACK_RECEIVE: /* a client has sent a message */
    
    if (write (pss->stdin,in,len) < len) {
      lwsl_err ("session-cgi: write error to CGI session %d handler's <stdin>.\n",pss->number);
      return (-1); /* close this session */
    }
    break;
    
  default:
    
    break;
  }
  
  return (0);
}

/*
// The remainder of this file is the boilerplate for compiling it as a standalone plugin.
*/

#define LWS_PLUGIN_PROTOCOL_SESSION_CGI		  \
  {						  \
    "session-cgi",				  \
    callback_session_cgi,			  \
    sizeof(struct per_session_data__session_cgi), \
    128,					  \
    0, NULL, 0					  \
  }

#if !defined (LWS_PLUGIN_STATIC)

static const struct lws_protocols protocols[] = {
  LWS_PLUGIN_PROTOCOL_SESSION_CGI
};

LWS_EXTERN LWS_VISIBLE int
init_protocol_session_cgi ( struct lws_context *context, struct lws_plugin_capability *c ) {
  
  if (c->api_magic != LWS_PLUGIN_API_MAGIC) {
    lwsl_err("Plugin API %d, library API %d", LWS_PLUGIN_API_MAGIC,
	     c->api_magic);
    return 1;
  }
  
  c->protocols = protocols;
  c->count_protocols = LWS_ARRAY_SIZE(protocols);
  c->extensions = NULL;
  c->count_extensions = 0;
  
  return 0;
}

LWS_EXTERN LWS_VISIBLE int
destroy_protocol_session_cgi ( struct lws_context *context) {
  return 0;
}
#endif

/*
// ================================================================================
// End of file.
// ================================================================================
*/
