/* -*- mode:c++ -*-
// ================================================================================
// GitLab:    http://gitlab.com/voidyourwarranty/session-cgi
// File:      application.c
// Date:      2018-10-10
// Author:    voidyourwarranty
// License:   Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
// Purpose:   Example CGI session handler
// ================================================================================
*/

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <poll.h> 

static int sigterm = 0; /* have we received a SIGTERM? */

void sig_handler ( int signo )
{
  if (signo == SIGTERM) {
    fprintf (stderr,"application: received SIGTERM.\n");
    sigterm = 1;
  }
}

/*
This program switches <stdin> to non-blocking input. Once per second, it tries to read data from <stdin>. If there is
any, it echoes the string that has be read to <stdout>, prefixed by some 'name'. If it reads the string "QUIT" or if
standard input reaches its end of file, the program quits. If it reads "NAME <something>" it prepends this name to any
echoed output. This program also outputs a standard line of text about every 10 seconds.

Note that if messages to <stdout> are written using <printf> rather than <write>, we need to make sure that we <flush>.
*/

int main ( void ) {

  int  n,j,k;
  char buffer[65536],name[65536],buff2[65536];

  strcpy (name,"unnamed");

  if (signal (SIGTERM,sig_handler) == SIG_ERR) {
    fprintf (stderr,"application: cannot handle SIGTERM.\n");
    return (-1);
  }
  
  if (fcntl (STDIN_FILENO,F_SETFL,O_NONBLOCK) < 0) {
    fprintf (stderr,"application: cannot set stdin to nonblocking.\n");
    return (-1);
  }

  j = 10;
  k = 1;
  do{

    n = read (STDIN_FILENO,buffer,sizeof(buffer)-1);
    if (n < 0) {
      if (errno != EAGAIN) {
	fprintf (stderr,"application: read error from stdin.\n");
	return (-1);
      }
    } else if (n == 0) {
      fprintf (stderr,"application: stdin eof.\n");
      return (0);
    } else {
      if (buffer[n-1] != '\n') { /* weird if the data does not end with a line feed */
	buffer[n] = 0;           /* we always terminate the string ... */
      } else {
	buffer[n-1] = 0;         /* ...and perhaps even remove the newline */
      }

      if (!strncmp (buffer,"NAME ",5)) {
	fprintf (stderr,"application: received name '%s' command.\n",&buffer[5]);
	strcpy (name,&buffer[5]);
      } else if (!strcmp (buffer,"QUIT")) {
	fprintf (stderr,"application: received quit command.\n");
	return (0);
      } else {
	sprintf (buff2,"%s echoes >%s<\n",name,buffer);
	if (write (STDOUT_FILENO,buff2,strlen (buff2)) < strlen (buff2)) {
	  fprintf (stderr,"application: write error.\n");
	  return  (-1);
	}
      }
    }

    /* Wait until more stdin arrives, but at most 1000 milli seconds */
    {
      struct pollfd pollme = { STDIN_FILENO, POLLIN, 0 };
      poll (&pollme,1,1000);
    }

    if (--j == 0) {
      j = 10;
      sprintf (buff2,"%s sends regular message no. %d.\n",name,k++);
      if (write (STDOUT_FILENO,buff2,strlen (buff2)) < strlen (buff2)) {
	  fprintf (stderr,"application: write error.\n");
	  return  (-1);
      }
    }
    
  }while (!sigterm);

  fprintf (stderr,"application: bye.\n");
  
  return (0);
}

/*
// ================================================================================
// End of file.
// ================================================================================
*/
