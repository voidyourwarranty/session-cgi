# session-cgi: A protocol for libwebsockets

This project is a plugin that provides a new protocol for [libwebsockets](https://libwebsockets.org/). It applies the
idea of the [Common Gateway Interface (CGI)](https://en.wikipedia.org/wiki/Common_Gateway_Interface) to the world of
[Web Sockets](https://en.wikipedia.org/wiki/WebSocket).

## Outline

The CGI was introduced during the 1990s and became the de-facto standard for the generation of dynamic web
pages. Whenever a client (web browser) requests a page from the web server, the web server spawns a new child process
and calls an executable (CGI script) that receives all parameters that the client provided as its input, among them, for
example, the URL, the PUT or GET parameters, etc. The CGI script then produces some standard output which forms the
dynamically generated web page that is sent back to the client. 

The CGI therefore separates the application logic from the web server and provides a standard interface between the two:
command line arguments, environment variables and standard input on the one hand, and standard output on the other hand.

The protocol `session-cgi` presented here, does the same for a web socket connection: Whenever a client (web browser)
upgrades an HTTP connection with the web server to a bidirectional web socket connection, the web server spawns a new
child process and calls an executable (*session handler*). This executable is kept running for the duration of the
session, i.e. until the client disconnects from the server, e.g. when the browser tab or window is closed. All web
socket messages that the client sends to the web server, are forwarded to the standard input of the *session handler*
while the standard output of the *session handler* is sent back over the web socket to the client.

`Session-cgi` therefore separates the application logic of a web socket application from the web server and provides a
standard interface between the two: standard input and standard output of the *session handler* process. The *session
handler* can be developed and tested independently of the web server, and this can be done in any programming language
that may be suitable. All the *session handler* needs to do is to wait for messages to arrive at its standard input,
then to process these and to output the response at its standard output.

This approach is an example of the Unix philosophy: develop programs each of which does only one thing and which does it
well. Then combine these programs using standardized interfaces.

## Versions

The present plugin was developed and tested with Version 3.0 (stable) of
[libwebsockets](https://libwebsockets.org/git/libwebsockets/tree/?h=v3.0-stable). Besides `libwebsockets`, it relies on
standard POSIX process control functions such as `fork()`, `pipe()`, `execv()`, `kill()` and on raw `read()` and
`write()` operations at file descriptor level. These are available on all standard Linux systems, but we have not
investigated whether it can be made to compile on Windows.

## Installation

### Prerequisites

We require [libwebsockets](https://github.com/warmcat/libwebsockets), select the branch `v3.0-stable`. Note that the
version that ships with the major Linux distributions may have been compiled with unsuitable build options (for example,
it may lack the web server `lwsws` that comes with `libwebsockets`), and so it is adviseable to compile the library from
source.

The subsequent installation instructions refer to some standard Ubuntu like Linux distribution. On Ubuntu 16.04.3 LTS,
we require CMake 2.8, OpenSSL, as well as
```
sudo apt-get install libuv1-dev libsqlite3-dev
```
### Installation of `libwebsockets`

Unpack `libwebsockets-3.0-stable.zip` into some project directory, say, `libwebsockets-3.0-stable`. 

Compiling with the option `LWS_WITHOUT_DAEMONIZE=0` didn't work in my case, and I had to edit the file
`libwebsockets-3.0-stable/test-apps/test-server.c`. I added line 27,
```
#define LOG_PERROR 7 // a daemon can never log more than that
```
and replaced line 367
```
syslog_options &= ~LOG_PERROR; ???
```
by
```
debug_level &= ~LOG_PERROR;
```
Then
```
cd libwebsockets-3.0-stable
mkdir build
cd build
cmake -DLWS_WITH_HTTP2=1 -DLWS_WITH_LWSWS=1 -DLWS_WITH_CGI=1 -DLWS_IPV6=1 -DLWS_WITH_PLUGINS=1 -DLWS_WITH_GENERIC_SESSIONS=1 -DLWS_WITH_PEER_LIMITS=1 -DLWS_WITH_ACCESS_LOG=1 -DLWS_WITH_SERVER_STATUS=1 -DLWS_WITHOUT_DAEMONIZE=0 ..
make
sudo make install
cd ../plugin-standalone/
mkdir build
cd build
cmake ..
make
sudo make install
```
The default installation directory is below `/usr/loca/`.

### Installation of `session-cgi`

In order to compile and install the `session-cgi` as a standalone plugin, we
```
cd session-cgi
mkdir build
cd build
cmake ..
make
sudo make install
```
This assumes that `session-cgi` is the name of the project directory. By default, the plugin is installed into the plugin directory
`<install-prefix>/share/libwebsockets-test-server/plugins/` of the test server that is included with `libwebsockets`. The shared 
library of the plugin is called `libprotocol_session_cgi.so`.

## Usage

The web server `lwsws` is configured by providing one file `/etc/lwsws/conf.d/<vhost>` for each virtual host. Let us
here build on the configuration of the test server on the local machine, port 7681, i.e.
```
"vhosts": [ {
  "name": "localhost",
  "port": "7681",
  <snip>
} ]
```
In order to initiate a web socket connection, we first serve a simple HTML page that contains the necessary javascipt in
order to upgrade the HTTP to web sockets. A sample file `index.html` is included in the project directory. We therefore
define a mount point for our vhost by
```
  <snip>
  "mounts": [ {
    "mountpoint": "/session-cgi",
    "origin":     "file:///<path-to-the-project-directory>",
    "default":    "index.html"
  },
  <snip>
  ]
```
Then we can point our web browser to `http://localhost:7681/session-cgi/index.html` in order to start a sample web
socket session. We include the new protocol by adding
```
  "ws-protocols": [ {
    "session-cgi" : {
      "status"  : "ok",
      "application" : "<path-to-the-project-directory>/build/application",
      "config" : "{ \"par1\" : \"val1\", \"par2\" : \"val2\" }"
    },
    <snip>
  } ]
```
Our protocol `session-cgi` uses two additional per-vhost configuration options:
- `application` is the full path to the *session handler* executable
- `config` is a JSON string that is sent to the *session handler* when a new session is created. Note that the value of `"config"` here
  is an ordinary string that desribes the JSON object. In particular all quotation marks (") that appear within the string need to be quoted.

The sample `application` included in the project directory echoes any message received from the web server. It also
regularly transmits unsolicited messages on its own. When it is sent a message of the form `NAME <something>` it
prepends the `<something>` to all echoed messages. When it is sent `QUIT`, it closes the connection by closing the pipes
that link it with the web server.

When, on the other hand, the client closes the browser tab or the browser window, the web server sends the *session
handler* a `SIGTERM`.

## Limitations

The user may run several sessions in different browser windows or browser tabs. The number of sessions from all clients
taken together is limited only by the memory and CPU resources that are required in order to run the various *session
handlers*. In practice, the operating system may also impose a limitation on the number of file descriptors that can be
open at any time. For each session, in addition to the socket that is used by the web server, we use three pipes for the
standard input, output and error of the *session handler*. In the current version of the protocol, we reject a session
only once the attemp to create a `pipe()` has failed. For a production server application, a more sophisticated
treatment might be required.

The present protocol does not provide any way in which different sessions can exchange information at the level of the
web server itself although other example protocols for `lwsws` demonstrate precisely that. As most real world
applications use some persistent data base, it is, of course, possible that different sessions interact by simply
accessing the same data base.

## Requirements on the *Session Handler*

As a general policy for the exchange of messages between the web server and the *session handler*, we work with
non-blocking input and with blocking output. I.e. we cannot be sure when additional input will arrive, but we write our
output whenever we please. The sample application demonstrates this. It uses `poll()` in order to wait until further
input has arrived, but from time to time it bluntly writes some output back.

Note that the application cannot be sure that any input it received consists of an entire message. The application is
responsible for concatenating the various pieces of input and for using appropriate delimiters in order to find the end
of the individual messages.

In particular, the `session-cgi` protocol uses an internal buffer size, and it chops up any messages it receives into
pieces of this size.

Once a session has been created and a new *session handler* process been started, the web server immediately sends it
three messages before any messages that might arrive from the client. All three are text representations of JSON objects
that begin with `{ "type": "something", ... }` and that end with a new line character. 

The first message has the type `"session"` and contains the session number (this is an internal number to the instance
of the web server that is incremented by one for each new session), the process ID of the *session handler*, the vhost
and port of the connection as well as `"peer"`, the IP address of the client as `libwebsockets` provides it in text
form. 

The second message has the type `"config"` and contains the corresponding string given in the per-vhost configuration of
the web server.

The third message has the type `"headers"` and contains all HTTP headers sent by the client when the connection was
upgraded to web sockets.

When you run the sample application, for every new session, these three JSON objects are immediately echoed.

Note that the internal session number transmitted with the `"session"` object is reset to 1 whenever the web server is
restarted. It can be used, for example, to match the log files of the server (where this number is routinely used in
order to identify the various sessions) with the log files of the *session handler*.

## License

[GNU General Public License GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
